function isNameValid(name = '') {
  const regex = /^[a-zA-Z-\s]{3,40}$/;
  return regex.test(name);
}

function isEmailValid(email = '') {
  const regex = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email.toLowerCase());
}

function isPasswordValid(password = '') {
  const regex = /(^[0-9]{6,63})+$/;
  return regex.test(password);
}

module.exports = {
  isEmailValid,
  isNameValid,
  isPasswordValid,
};
