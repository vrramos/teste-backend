const utils = require('../services/utils');

const invalidLogin = fn => async (req, res, next) => {
  try {
    await fn(req, res, next);
  } catch (err) {
    if (err.message === 'EmailOrPassordInvalid')
      return res.status(400).json({ message: 'Email or Password Invalid' });

    return res.status(500).json({ error: err.name });
  }
};

function validLoginMiddleware(req, res, next) {
  const { email, password } = req.body;

  if (!utils.isEmailValid(email) || !utils.isPasswordValid(password))
    return res.status(400).json({ message: 'Invalid Fields' });

  return next();
}

module.exports = {
  validLoginMiddleware,
  invalidLogin,
};
