const { tokenValid } = require('../services/jwt');

const userValidMiddleware = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) return res.status(401).json({ message: 'Access Denied' });

  const payload = tokenValid(token);

  if (req.params.id !== payload.user_id)
    res.status(401).json({ message: "User Invalid." })

  return next();
};

module.exports = {
  userValidMiddleware,
};
