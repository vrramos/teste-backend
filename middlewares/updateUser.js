const utils = require('../services/utils');
const { tokenValid } = require('../services/jwt');

const updateNameMiddleware = (req, res, next) => {
  const { name } = req.body;
  const token = req.headers.authorization;

  if (!token) return res.status(401).json({ message: 'Access Denied.' });

  const payload = tokenValid(token);

  if (req.params.id != payload.id_user)
    return res.status(400).json({ message: "You not authorized to update this user." })

  if (!utils.isNameValid(name)) return res.status(400).json({ message: 'Invalid name' });

  return next();
}

module.exports = { updateNameMiddleware };
