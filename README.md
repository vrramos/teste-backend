# Configurando ambiente
- Na raiz do projeto, rode o comando **npm i** para instalar as dependências.
- Crie um arquivo .env na raiz do projeto e insire as informações para a conexão com o MySql, criptografia e autenticação via Jwt:
  DB_USER=root,
  DB_PASS=password,
  DB_NAME=ioasys_imdb,
  DB_HOST=127.0.0.1,
  SECRET_KEY_CRYPTO=senhasecreta,
  SECRET_KEY_JWT=senhamegasecreta.

# Banco de Dados
- Crie um banco de dados MySql com o nome **ioasys_imdb**.
- Rode o comando **npx sequelize db:migrate** na raiz do projeto para criar a tabela de usuário.
- Rode o comando **npx sequelize db:seed:all** na raiz do projeto para popular o banco com usuários padrãos.

- Caso queira reverter as ações acima, utilize os seguintes comandos.
  
  - **npx sequelize db:migrate:undo:all** para reverter as tabelas.
  - **npx sequelize db:seed:undo:all** para reverter os usuários padrão padrãos.


# Documentação das Rotas
# Post /user
- O name deve ter caracteres maiores que 3 e menores que 40.
- O email deve estar no modelo teste@teste.com.
- O password deve ser apenas números e terem digitos maiores o iguais a 6. 
- O role deve ser apenas client ou admin. 

- O body da requisição deve ter o seguinte formato:
```json
{
  "username": "nome-de-Usuário",
  "email": "email@mail.com",
  "password": "123456",
  "role": "client"
}
```
**obs: Lembrando que o role pode ser apenas client ou admin.**

- Caso tudo esteja correto a seguinte resposta é retornada:
```json
  {
    "message": "Registered Successfully."
  }
```
- Caso algum campo esteja inválido, terá a seguinte resposta:
```json
  {
    "message": "Invalid Fields"
  }
```
# Post /login
- O body da requisição deve ter o seguinte formato:
```json
{
  "email": "email@mail.com",
  "password": "123456"
}
```

- Caso tudo esteja correto a seguinte resposta é retornada:
```json
  {
    "name": "Herenildo Testando o Teste",
    "email": "teste@teste.com",
    "role": "client",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSGVyZW5pbGRvIFRlc3RhbmRvIG8gVGVzdGUiLCJlbWFpbCI6InRlc3RlQHRlc3RlLmNvbSIsInJvbGUiOiJjbGllbnQiLCJpZF91c2VyIjozLCJpYXQiOjE1OTkzMzgxODcsImV4cCI6MTU5OTU5NzM4N30.pyT-vNT9sQ6i2VMNm0OTfrrUO0eEQgONgQnA6CnQv4E"
  }
```
- Caso algum campo esteja inválido, terá a seguinte resposta:
```json
  {
    "message": "Invalid Fields"
  }
```

# Put /user/:id
- Nessa entrega apenas o campo name poderá ser atualizado.
- Para fazer a atualização do nome, o usuário deve estar logado e com a autenticação ativa.
- Caso não coloque o token no header da requisição terá a seguinte resposta:
```json
{
  "message": "Access Denied."
}
```

- Caso tente deletar algum usuário que não seja você:
```json
{
  "message": "User Invalid."
}
```

- O body da requisição deve ter o seguinte formato:
```json
{
  "name": "Anna Cecilia G",
}
```

- O nome deve ser uma string, com 3 a 40 caracteres e sem caracteres especiais. Caso coloque um nome inválido, essa será a resposta:
```json
{
  "message": "Invalid name"
}
```

- Caso tudo esteja correto, terá a seguinte resposta:
```json
{
  "message": "Information Updated Successfully"
}
```
# Delete /user/:id

- Caso não coloque o token no header da requisição terá a seguinte resposta:
```json
{
  "message": "Access Denied."
}
```

- Caso tente deletar algum usuário que não seja você:
```json
{
  "message": "User Invalid."
}
```

- Se tudo ocorrer bem, um status 200 será retornado.


# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🏗 O que fazer?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- A API deverá ser construída em **NodeJS** ou **Rails**
- Implementar autenticação e deverá seguir o padrão **JWT**, lembrando que o token a ser recebido deverá ser no formato **Bearer**
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **SailsJS**
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM**
- Bancos relacionais permitidos:
  - MySQL
  - MariaDB
  - Postgre
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Segurança da API, como autenticação, senhas salvas no banco, SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver)
- Migrations para a criação das tabelas do banco relacional

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Testes unitários
- Linter
- Code Formater

**Obs.: Lembrando que o uso de algum linter ou code formater irá depender da linguagem que sua API for criada**

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

- Admin

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Usuário

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Filmes

  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro)
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não**

# 🔗 Links

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html
