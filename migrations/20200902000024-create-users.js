module.exports = {
  up: async (queryInterface, Sequelize) => (
    queryInterface.createTable('Users', {
      id_user: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
      },
      role: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      user_active: {
        allowNull: false,
        type: Sequelize.TINYINT,
        defaultValue: 1,
      }
    })
  ),

  down: async queryInterface => queryInterface.dropTable('Users'),
};
