const UserRepository = require('../repository/UserRepository');
const { createJWT } = require('../services/jwt');

exports.registerUser = async (req, res) => {
  await new UserRepository().createUser(req.body);
  res.status(200).json({ message: "Registered Successfully." });
}

exports.login = async (req, res) => {
  const login = await new UserRepository().login(req.body);

  const { name, email, role, id_user } = login;
  const token = createJWT({ name, email, role, id_user });

  return res.status(200).json({ name, email, role, token });
};

exports.userUpdate = async (req, res) => {
  const token = req.headers.authorization;
  await new UserRepository().updateUserInformation(req.body, token);

  return res.status(200).json({ message: "Information Updated Successfully" })
}

exports.deleteUser = async (req, res) => {
  const token = req.headers.authorization;
  await new UserRepository().desativeUser(token);

  return res.status(200).json({ message: "Successfully deleted" });
}
