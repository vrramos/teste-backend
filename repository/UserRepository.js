const { User } = require('../models');
const { encrypt, decrypt } = require('../services/crypto');
const { tokenValid } = require('../services/jwt');

class UserRepository {
  async createUser(userInfo) {
    const { password, ...info } = userInfo;

    const encryptPassword = encrypt(password);
    await User.create({ password: encryptPassword, ...info });
  }

  async _loginValidEmail(email, password) {
    const findEmail = await User.findOne({ where: { email } });
    const unencryptedPassword = decrypt(findEmail.password);
    if (unencryptedPassword !== password) throw new Error('EmailOrPassordInvalid');

    return findEmail;
  }

  async login({ email, password }) {
    return await this._loginValidEmail(email, password);
  }

  _update(name, token) {
    const { id_user, email } = tokenValid(token);

    const update = User.update({ name }, { where: { id_user, email } });
    return update;
  }

  async updateUserInformation({ name }, payload) {
    const updateUser = await this._update(name, payload);
    return updateUser;
  }

  async desativeUser(token) {
    const { id_user } = tokenValid(token);
    await User.update({ user_active: 0 }, { where: { id_user } });
  }
}

module.exports = UserRepository;
