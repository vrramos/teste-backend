module.exports = {
  up: async queryInterface => (
    queryInterface.bulkInsert(
      'Users',
      [
        {
          email: 'victor@gmail.com',
          name: 'Victor Felipe',
          password: 'U2FsdGVkX19/RSSH86lYnIgFjZmgWTeM/0P4QSRAfyo=',
          role: 'admin',
          user_active: 1,
        },
        {
          email: 'cecilia@gmail.com',
          name: 'Anna Cecília',
          password: 'U2FsdGVkX19/RSSH86lYnIgFjZmgWTeM/0P4QSRAfyo=',
          role: 'client',
          user_active: 1,
        },
      ],
      {},
    )
  ),

  down: async queryInterface => queryInterface.bulkDelete('Users', null, {}),
};
