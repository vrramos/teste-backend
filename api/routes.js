const { login, registerUser, userUpdate, deleteUser } = require('../controllers/userController');

module.exports = {
  login,
  registerUser,
  userUpdate,
  deleteUser,
}
