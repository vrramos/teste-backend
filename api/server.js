const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const { validRegisterMiddleware } = require('../middlewares/registerMiddleware');
const { validLoginMiddleware, invalidLogin } = require('../middlewares/loginMiddleware');
const { updateNameMiddleware } = require('../middlewares/updateUser');
const { userValidMiddleware } = require('../middlewares/userValidMiddleware');

const { login, registerUser, userUpdate, deleteUser } = require('./routes');

app.post('/user', validRegisterMiddleware, registerUser);
app.post('/login', validLoginMiddleware, invalidLogin(login));
app.put('/user/:id', updateNameMiddleware, userUpdate);
app.delete('/user/:id', userValidMiddleware, deleteUser);

app.use('*', (_req, res) => res.status(404).json({ message: 'Page not found' }));

module.exports = app;
